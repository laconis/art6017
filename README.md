# ART6017 atelier Internet et web

Bienvenu dans l'atelier ART6017. Vous trouverez ici toutes les ressources pour suivre l'atelier et faire les exercices.

Avant de venir à l'atelier, merci [de vous créer un compte sur Framagit](https://framagit.org/users/sign_in?redirect_to_referer=yes) (et de m'envoyer votre username) et [d'installer les outils nécessaires](./parcours/03_outils.md).

## Plan de cours

### Samedi 3 novembre

1. Introduction générale ([parcours/01_intro](./parcours/01_intro.md))
2. Introduction à l'environnement web ([parcours/02_web](./parcours/02_web.md))
3. Installation des outils ([parcours/03_outils](./parcours/03_outils.md))
4. HTML - premiers pas ([parcours/04_html](./parcours/04_html.md))

### Dimanche 4 novembre

1. HTML - organiser ses pages ([parcours/04bis_html](./parcours/04bis_html.md))
2. CSS - premiers pas ([parcours/05_css](./parcours/05_css.md))
3. CSS Framework ([parcours/06_framework](./parcours/06_framework.md))
4. Projet - mise en place ([parcours/07_projet](./parcours/07_projet.md))

### Dimanche 11 novembre

1. Debbuging des projets ([parcours/08_debug](./parcours/08_debug.md))
2. Présentation des projets ([parcours/09_presentation](./parcours/09_presentation.md))
3. CMS - introduction ([parcours/10_cms](./parcours/10_cms.md))

## Ressources générales

- [developer.mozilla.org](https://developer.mozilla.org/fr/docs/Apprendre) : tutoriels pour démarrer la programmation web.
- [w3schools.com](https://www.w3schools.com/) : ressource complète du [W3C](https://www.w3.org/) pour toute la programmation web : documentation, exemples, tutoriels
