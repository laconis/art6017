
# liste projets

n°|nom|username|projet|url
:--|:--|:--|:--|:--|:--
1  | Alexandra Moreno |	morenoalexe|https://framagit.org/morenoalexe/art6017-alexandra | https://morenoalexe.frama.io/art6017-alexandra/
2  | Andrée-Anne Mercier |	 - |https://framagit.org/andreeannemercier/art6017-andreeanne | https://andreeannemercier.frama.io/art6017-andreeanne/
3  | Antoine Francis |	antojne|https://framagit.org/antojne/art6017-antoine | https://antojne.frama.io/art6017-antoine/
4  | Azar Amiri Yeganeh |	Lia_amiri|https://framagit.org/Lia_amiri/art6017-lia | https://Lia_amiri.frama.io/art6017-lia/
5  | Charlotte Maheu |	Charloue |https://framagit.org/Charloue/art60147-charlotte | https://Charloue.frama.io/art60147-charlotte/
6  | Clara Daeninck | claradaeninck|https://framagit.org/claradaeninck/art6017-clara | https://claradaeninck.frama.io/art6017-clara/
7  | David Prévost-Brisson |	davidpbrisson|https://framagit.org/davidpbrisson/art6017_david | https://davidpbrisson.frama.io/art6017_david/
8  | Frédérique Bordeleau |	fredbordel|https://framagit.org/fredbordel/art6017-frederique | https://fredbordel.frama.io/art6017-frederique/
9  | Igor Dubois |	IGZ2378|https://framagit.org/IGZ2378/art6017-igor | https://IGZ2378.frama.io/art6017-igor/
10 | Julie Francoeur |	JulieFrancoeur|https://framagit.org/JulieFrancoeur/art6017-julie | https://JulieFrancoeur.frama.io/art6017-julie/
11 | Karyann Pilon |	kay| -
12 | Sandrine Cadieux |	SandrineCadieux|https://framagit.org/SandrineCadieux/art6017-sandrine | https://SandrineCadieux.frama.io/art6017-sandrine/
13 | Yao LiYao Li	| 2leeyao | https://framagit.org/2leeyao/art6017-yaoli | https://2leeyao.frama.io/art6017-yaoli/
