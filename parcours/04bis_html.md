# Organiser une page web

Le standard HTML5 est apparu en 2014. Il succède à plusieurs versions du standard HTML depuis 1990.  
[En savoir plus sur Wikipédia](https://fr.wikipedia.org/wiki/HTML5)

HTML5 introduit de nouveaux éléments plus sémantiques que les versions précédentes.

En pratique, cela ne change pas grand chose, on continue de construire la page par bloc (`<div>`), mais ces blocs peuvent être plus facilement identifiés, car ils sont signifiants.

Quelques exemples de balises "sémantique"[^1] :

- `<main>` : définit le contenu principal de la page, il doit être unique dans la page.
- `<section>` : définit les sections du document, telles que les chapitres, en-têtes, pieds de page.
- `<article>` : partie indépendante du site, comme un commentaire.
- `<aside>` : associé à la balise qui le précède.
- `<header>` : spécifie une introduction, ou un groupe d'éléments de navigation pour le document.
- `<footer>` : définit le pied de page d'un article ou un document. Contient généralement le nom de l'auteur, la date à laquelle le document a été écrit et / ou ses coordonnées.
- `<nav>` : définit une section dans la navigation.
- `<figure>` : définit des images, des diagrammes, des photos, du code, etc.
- `<figcaption>` : légende pour la balise `<figure>`.
- `<audio>` : pour définir un son, comme la musique ou les autres flux audio (streaming).
- `<video>` : permet d’insérer un contenu vidéo en streaming.

Je vous invite maintenant à consulter [les parties principales d'un document (Mozilla)](https://developer.mozilla.org/fr/docs/Apprendre/HTML/Introduction_%C3%A0_HTML/Document_and_website_structure#Principales_parties_d'un_document).

![site web typique](https://mdn.mozillademos.org/files/15971/ecran.png) _Site web typique_

[^1]: source [Wikipédia](https://fr.wikipedia.org/wiki/HTML5#Nouveaux_%C3%A9l%C3%A9ments)

------

# Organiser son travail

Nous allons exploiter ce qu'on vient d'apprendre pour créer une page d'accueil (`index.html`) pour organiser (et pouvoir naviguer) les différentes pages de votre site.

Pour cela, créer un nouveau fichier `index.html`.

On va y mettre la structure suivante ([source](https://developer.mozilla.org/fr/docs/Apprendre/HTML/Introduction_%C3%A0_HTML/Document_and_website_structure#Apprentissage_actif_exploration_du_code_de_l'exemple)) :

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
<!-- <link rel="stylesheet" href="style/mystyle.css"> -->

    <title>Page d'accueil de Nicolas</title>

  </head>

  <body>
    <!-- Voici notre en‑tête principale utilisée dans toutes les pages
         de notre site web -->

    <header>
      <h1>En-tête</h1>
    </header>

    <nav>
      <ul>
        <li><a href="page01.html">page01</a></li>
        <li><a href="page02.html">page02</a></li>
        <li><a href="projet/index.html">projet</a></li>
      </ul>
    </nav>

    <!-- Ici nous mettons le contenu de la page -->
    <main>

      <!-- Il contient un article -->
      <article>
        <h2>À propos de ce site</h2>

        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Donec a diam lectus. Set sit amet ipsum mauris. Maecenas congue ligula as quam viverra nec consectetur ant hendrerit. Donec et mollis dolor. Praesent et diam eget libero egestas mattis sit amet vitae augue. Nam tincidunt congue enim, ut porta lorem lacinia consectetur.</p>

        <h3>À propos de cet atelier</h3>

        <p>Donec ut librero sed accu vehicula ultricies a non tortor. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aenean ut gravida lorem. Ut turpis felis, pulvinar a semper sed, adipiscing id dolor.</p>

        <p>Pelientesque auctor nisi id magna consequat sagittis. Curabitur dapibus, enim sit amet elit pharetra tincidunt feugiat nist imperdiet. Ut convallis libero in urna ultrices accumsan. Donec sed odio eros.</p>

        <h3>À propos de rien</h3>

        <p>Donec viverra mi quis quam pulvinar at malesuada arcu rhoncus. Cum soclis natoque penatibus et manis dis parturient montes, nascetur ridiculus mus. In rutrum accumsan ultricies. Mauris vitae nisi at sem facilisis semper ac in est.</p>

        <p>Vivamus fermentum semper porta. Nunc diam velit, adipscing ut tristique vitae sagittis vel odio. Maecenas convallis ullamcorper ultricied. Curabitur ornare, ligula semper consectetur sagittis, nisi diam iaculis velit, is fringille sem nunc vet mi.</p>
      </article>

      <!-- Le contenu « à côté » peut aussi être intégré dans le contenu
           principal -->
      <aside>
        <h2>En relation</h2>

        <ul>
          <li><a href="https://framagit.org/username/art6017-prenom">Source de mon site</a></li>
          <li><a href="https://framagit.org/laconis/art6017">vers l'atelier Internet et web</a></li>
          <li><a href="https://developer.mozilla.org/fr/docs/Apprendre">Apprendre HTML avec Mozilla</a></li>
          <li><a href="https://www.w3schools.com/">W3School</a></li>
        </ul>
      </aside>

    </main>

    <!-- Et voici notre pied de page utilisé sur toutes les pages du site -->

    <footer>
      <p>copyright ou [copyleft](http://creativecommons.org/) ? - 2018</p>
    </footer>

  </body>
</html>
```

**Modifiez le contenu des liens pour pointer correctement vers vos différentes pages.**

Puis dans le terminal :

```bash
git add index.html
git commit -m "ajout d'un index"
git push
```

Vous pouvez maintenant visitez la page `https://laconis.frama.io/art6017-nicolas/`. Notez que les deux URLs suivantes sont équivalentes :

- `https://laconis.frama.io/art6017-nicolas/`
- `https://laconis.frama.io/art6017-nicolas/index.html`

**Par défaut, lorsqu'une URL s'arrête sur un dossier, le navigateur cherche un fichier `index.html`.** S'il n'en trouve pas, il affiche une erreur 404 - Page not found.

Votre page est toute croche ? Il lui manque les styles. Passons à [la section suivante](parcours/05_css.md).

------
