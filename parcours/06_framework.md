# Frameworks

Un _framework_ est
«un ensemble cohérent de composants logiciels structurels, qui sert à créer les fondations ainsi que les grandes lignes de tout ou d’une partie d'un logiciel (architecture)»[^1].

Un _framework CSS_ consiste en une série de classes spécialement définies pour agencer et styler des documents HTML. Les fournisseurs de framework CSS proposent en général des _templates_ HTML, c'est-à-dire, des pages pré-structurées (et pré-classées) afin de pouvoir fonctionner avec le _framework CSS_.

Ces framework CSS sont très pratiques, car ils constituent une base de travail pour produire son propre site sans avoir à tout faire soi-même.

Au choix :
  - le framework [W3.CSS](https://www.w3schools.com/w3css/default.asp) : fonctionne sans javascript
  - le framework [Bootstrap](https://www.w3schools.com/bootstrap/default.asp) : certains templates Bootstrap pourront utiliser du javascript.

Lorsqu'on utiliser un framework, une bonne pratique est de ne pas modifier les feuilles de style initiales, mais d'ajouter sa propre feuille de style (par exemple `mystyle.css`) qui viendra s'ajouter et modifier les styles du framework à l'affichage.  
Explications pendant l'atelier..


------
[^1]: source [Wikipédia](https://fr.wikipedia.org/wiki/Framework)
