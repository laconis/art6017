# Le projet

L'atelier est évalué à 80% sur la réalisation d'un projet de site web. Ce site peut consister en une page unique, ou plusieurs pages liées à partir d'une page d'index.

Vous devez utiliser [l'un des deux frameworks](parcours/06_framework.md) pour réaliser votre projet. Pour cela, utiliser un template (ou plusieurs) existant.s pour élaborer votre site/page.

Le projet devra se trouver dans un sous-dossier `projet` de votre répertoire actuel, accessible à partir d'un fichier `index.html`.

Le projet sera donc accessible sur l'url :

    https://laconis.frama.io/art6017-nicolas/projet/

## Structure du répertoire

Soit la structure de votre répertoire :

```
__art6017-nicolas/
      |--index.html : la page d'accueil
      |--page01.html : la première page
      |--images/  : le dossier contenant d'éventuelles images
      |--styles/  : le dossier de feuilles de style
            |--style01.css : la feuille de style pour page01.html  
            |--mystyle.css : la feuille de style pour index.html
      |--projet/ : le sous-dossier du projet  
            |--index.html : la page d'accueil du projet
            |--mapage.html : d'éventuelles autres pages HTML
            |--styles/ : le dossier de feuilles de style du projet
```
