# Introduction générale

**Objectifs de l'atelier:**

- comprendre comment fonctionne le web
- vous rendre autonome pour démarrer des projets web
- savoir trouver l'information qu'il vous manque
- savoir publier

**Fonctionnement de l'atelier**

- ressource principale : https://framagit.org/laconis/art6017
- un wiki pour collaborer : https://framagit.org/laconis/art6017/wikis/home

**Evaluation**

- 20% : présence et participation
- 80% : projet, dont:
  - 20% : compréhension
  - 25% : réalisation
  - 25% : fonctionnel
  - 10% : suivi de version
- bonus : déploiement en ligne

Le projet est un travail individuel. Il s'agira de produire une page ou un mini-site web vous présentant, dans une forme libre.

Si vous êtez déjà avancés en programmation HTML/CSS, quelques exercices possib
