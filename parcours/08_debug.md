# Debugging

## arborescence et chemin de fichier (_path_)

Rappelez vous la structure du répertoire :

```
__art6017-nicolas/
      |--index.html : la page d'accueil
      |--page01.html : la première page
      |--images/  : le dossier contenant d'éventuelles images
      |--styles/  : le dossier de feuilles de style
            |--style01.css : la feuille de style pour page01.html  
            |--mystyle.css : la feuille de style pour index.html
      |--projet/ : le sous-dossier du projet  
            |--index.html : la page d'accueil du projet
            |--mapage.html : d'éventuelles autres pages HTML
            |--styles/ : le dossier de feuilles de style du projet
```

    - `/` désigne "la racine" de l'arborescence. À la ligne de commande, il s'agit de la racine de l'ordinateur, ou du disque dur. Dans un projet web, il s'agira de la racine du site.
    -  `.` désigne le répertoire actuel (là où se trouve votre fichier)
    -  `..` désigne le répertoire parent (le répertoire ou dossier dans lequel se trouve le répertoire actuel)
    - `./projet/` la barre oblique permet d'atteindre (ou de rentrer dans) le répertoire enfant, ici dans le répertoire `projet`.
    - ainsi la source (`src`) de cette image `<img src="./images/monimage.jpg">` sera "l'image `monimage.jpg` dans le répertoire `images` depuis le répertoire actuel".
    - dans `<img src="/projet/images/monimage.jpg">`, la source sera "l'image `monimage.jpg` dans le répertoire `images` dans le répertoire `projet` à la racine du site web".

Pour naviguer dans l'arborescence avec la ligne de commande :

- `cd projet` : rentrer dans le répertoire `projet` enfant du répertoire actuel
- `cd Desktop/art6017-nicolas/projet` : accéder directement au répertoire `projet` depuis le dossier personnel (par exemple)
- `cd ..` : remonter dans l'arborescence d'un répertoire
- `cd .` : ne fait rien, puis `.` désigne le répertoire en cours.
- `cd /` : remonte à la racine de l'ordinateur.

## including HTML in HTML

Permet de séparer le contenu récurrent (apparait sur toutes les pages) du contenu spécifique (dédié à la page en cours).

Exemple : vous souhaitez avoir la même barre de navigation dans toutes vos pages. Dans une page (la page `index.html` par exemple), identifiez le code html de la barre de navigation :

```html
    <nav>
      <ul>
        <li><a href="page01.html">page01</a></li>
        <li><a href="page02.html">page02</a></li>
        <li><a href="projet/index.html">projet</a></li>
      </ul>
    </nav>
```

1. Vous collez ce code dans un fichier `navigation.html` (par exemple).
2. Ensuite, dans vos différentes pages, à la place de la navigation, vous intégrez le code :

```html
<div w3-include-html="navigation.html"></div>
```

3. Puis vous créez un fichier `include.js` dans lequel vous collez ce code:

```javascript
 <script>
function includeHTML() {
  var z, i, elmnt, file, xhttp;
  /*loop through a collection of all HTML elements:*/
  z = document.getElementsByTagName("*");
  for (i = 0; i < z.length; i++) {
    elmnt = z[i];
    /*search for elements with a certain atrribute:*/
    file = elmnt.getAttribute("w3-include-html");
    if (file) {
      /*make an HTTP request using the attribute value as the file name:*/
      xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
        if (this.readyState == 4) {
          if (this.status == 200) {elmnt.innerHTML = this.responseText;}
          if (this.status == 404) {elmnt.innerHTML = "Page not found.";}
          /*remove the attribute, and call this function once more:*/
          elmnt.removeAttribute("w3-include-html");
          includeHTML();
        }
      }
      xhttp.open("GET", file, true);
      xhttp.send();
      /*exit the function:*/
      return;
    }
  }
}
</script>
```

4. Puis dans chacune de vos pages où vous avez ajouté une balise `<div w3-include-html="...">`, vous ajoutez dans la balise `<head>` la référence au script : `<script src="include.js"></script>`
5. Et enfin, en fin de fichier html avec la balise `<body>`, vous ajouter ces 3 lignes :

    ```html
    <script>
      includeHTML();
    </script>
    ```

Soit au final, une structure de fichier html similaire à celle-ci :

```html
 <!DOCTYPE html>
<html>
<head>
  <!-- toutes vos balises de head : meta, link, etc. -->

  <!-- ici, on intègre le script à la page (étape 4) -->
  <script src="include.js"></script>
</head>
<body>

  <!-- pour intégrer la navigation à cet endroit (étape 2) -->
  <div w3-include-html="navigation.html"></div>

  <!-- le reste de votre contenu html  -->

  <!-- à la fin du fichier, on lance le script (étape 5)-->
  <script>
    includeHTML();
  </script>

</body>
</html>
```


- Voir l'explication complète : https://www.w3schools.com/howto/howto_html_include.asp
- En utilisant la ressource w3.js : https://www.w3schools.com/w3js/w3js_html_include.asp

## questions générales

  - préparation des sources (images, vidéo, sons, textes) : réduire et compresser.
  - **séparer les styles** : lorsque vous utilisez un template ou un style existant, et que vous souhaitez l'adapter, prenez l'habitude de créer votre propre fichier de style plutôt que d'éditer le template ou style initial.
