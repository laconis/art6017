# Quelques outils pour travailler

Pour l'atelier vous aurez besoin d'amener votre propre ordinateur (indispensable) et d'installer quelques outils pour travailler ensemble.

## un éditeur de texte/code (indispensable)

Les éditeurs de code sont plus ou moins simples selon les fonctionnalités qu'ils offrent. À minima pour l'atelier, l'éditeur devra pouvoir colorer les syntaxes html et css (ce qu'ils font tous).

Suggestion : [Atom.io](https://atom.io/) qui fonctionnera sur toutes les plateformes.

Voir d'autres éditeurs sous [Windows](https://alternativeto.net/software/atom/?platform=windows), [Max](https://alternativeto.net/software/atom/?platform=mac), [Linux](https://alternativeto.net/software/atom/?platform=linux).

## un compte sur framagit.org (recommandé)

Sur [Framagit.org](https://framagit.org/users/sign_in?redirect_to_referer=yes), créez vous un compte. Cela vous sera utile pour :
1. partager votre code avec moi (et la classe si vous le souhaitez),
2. publier votre page web en ligne,
3. [participer au wiki](https://framagit.org/laconis/art6017/wikis/home) de ce répertoire.

Cliquez sur le bouton [Request Access] pour demander un accès au répertoire.

**Merci de m'envoyer ensuite votre username.**


## un gestionnaire de version : git (recommandé)

Le gestionnaire de version git va vous permettre de communiquer votre code à Framagit. Nous verrons ensemble comment l'utiliser.

- Linux: déjà installé
- Mac: déjà installé sinon, dans un terminal :
  - installer homebrew : `/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`
  - installer git avec homebrew : `brew install git`
- Windows : [installez git pour Windows](https://git-scm.com/downloads)

## un serveur (recommandé)

Selon votre projet, un serveur local ne sera pas forcément nécessaire. Mais il peut être utile dans le cas où votre projet se complexifie et fait appel à certaines ressources.

Le principe d'un serveur local est de "servir" les fichiers de votre page/site web et de les rendre accessibles à votre navigateur sur une url particulière `http://localhost`, comme si les données se trouvaient sur un vrai serveur.

En général, un ["port"](https://fr.wikipedia.org/wiki/Port_(logiciel)) est spécifié pour associer votre site à une adresse url plus précise, par exemple : `http://localhost:8080/` qui redirigera spécifiquement sur le dossier où vous travailler. Nous verrons cela ensemble.

- Linux : déja installé - ([lancer un serveur avec python](https://www.pythonforbeginners.com/modules-in-python/how-to-use-simplehttpserver/))
- Mac : déja installé - ([lancer un serveur avec python](https://www.pythonforbeginners.com/modules-in-python/how-to-use-simplehttpserver/)). Si pas installé
- Windows : [installer easyPHP webserver](http://www.easyphp.org/easyphp-webserver.php)


## Utiles au fil de l'eau

- les recommandations d'outils de Mozilla : https://developer.mozilla.org/fr/docs/Apprendre/Commencer_avec_le_web/Installation_outils_de_base
- https://techarea.fr/homebrew-gestionnaire-paquets-macos/
- https://www.makeuseof.com/tag/install-pip-for-python/
