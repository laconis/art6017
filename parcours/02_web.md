# Introduction au web et à Internet

Voir [les slides](./media/ART6017-introduction.pdf) de Joana Casenave ([CC-BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr))


Récap :

1. Internet est un ensemble d'infrastructures physiques et un ensemble de protocoles pour permettre aux machines de communiquer
2. le Web est une ensemble de documents et de données interreliés, et un ensemble de protocoles permettant de lire et d'écrire ces documents.
3. Le Web est accessible via les navigateurs.
4. Les navigateurs interprêtent des ressources web écrites dans les langages HTML, CSS, JS pour les afficher sous forme de pages web.
  - HTML structure la page et l'information
  - CSS met en forme la page et l'information
  - JS est capable de manipuler les informations de la page et d'ajouter de l'interactivité.


à montrer :
- encapsulation des paquets/protocole TCP/IP/HTTP > wikipedia
- Routeur ?
- protocole HTTP sur wikipedia
