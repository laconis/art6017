# Pour allez plus loin

## Site statique

  - html + css + js + sources (images)  

## Générateur de site statique :

  1. créer du contenu dans des fichiers (html, mardown, asciidoc)
  2. générer le site à partir du contenu : "build" le cms construit le site
  3. publier le site qui a été construit > déposer sur un serveur.

Exemple: Jekyll, Hugo, Hexo, Middleman

## CMS de site dynamique

CMS: _Content Management System_ ou Système de Gestion de Contenu

[Exemples de CMS](https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_gestion_de_contenu#Liste_de_syst%C3%A8mes_de_gestion_de_contenu) : Wordpress, SPIP, Django, WIX, Mediawiki, etc.

- associé à une base de données
- les contenus sont stockés dans la base de données
- les pages html sont produites à la volée grâce à des scripts (programmes informatiques). Selon les CMS, les scripts peuvent être écrits en différents langages de programmation : python (CMS Django), php (CMS Drupal, Wordpress ou SPIP), ruby, etc. Ces languages permettent de communiquer avec la base de données.


## SEO

- Définition SEO ou [Optimisation pour les moteurs de recherche (Wikipédia)](https://fr.wikipedia.org/wiki/SEO)
- Ressources Google : [Bien débuter en référencement naturel (SEO)](https://support.google.com/webmasters/answer/7451184?hl=fr)
