# Premiers pas en CSS

## Styler page01.html

Dans votre répertoire de travail, créer un nouveau dossier `styles`, puis, dans ce nouveau dossier, créer un nouveau fichier `style01.css`.

Ouvrez ce fichier `style01.css` dans Atom, et copiez le code suivant :

```css
h1 {
  color: blue;
  background-color: yellow;
  border: 1px solid black;
}

p {
  color: red;
}
```

Vous venez de créer un style pour votre site. Il faut maintenant associer votre document `page01.html` avec ce style `style01.css`.

Pour cela, ouvrez dans Atom le document `page01.html` et ajoutez quelque part dans l'élement `<head>` la ligne suivante :

```html
<link rel="stylesheet" href="styles/style01.css">
```

Soit aprés l'opération, le début du document `page01.html` :

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="style01.css">
    <title>page01 - Ma page de test</title>
  </head>
  <body>
...
```

Vous pouvez maintenant ouvrir le document `page01.html` dans votre navigateur.

Dans le terminal, nous allons ajouter ces modifications au registre :

```bash
git add .
git commit -m "ajout d'une feuille de style pour page01.html"
```


## Quelques bases avant d'aller plus loin

- feuille de style interne, externe ou _"in-line"_ ? ([comment appliquer les CSS au HTML (Mozilla)](https://developer.mozilla.org/fr/docs/Apprendre/CSS/Introduction_%C3%A0_CSS/Le_fonctionnement_de_CSS#Comment_appliquer_les_CSS_aux_HTML))
- [la syntaxe CSS (sur Mozilla)](https://developer.mozilla.org/fr/docs/Apprendre/CSS/Introduction_%C3%A0_CSS/La_syntaxe#Un_peu_de_vocabulaire)
- les sélecteurs : voir [les slides de Joana](media/ART6017-CSS.pdf) et [les sélecteurs sur Mozilla](https://developer.mozilla.org/fr/docs/Apprendre/CSS/Introduction_%C3%A0_CSS/Les_s%C3%A9lecteurs)


type | HTML | CSS | sélection
:--  | :-:  | :-: | :--
éléments |  `<div>` | `div {}` | tous les élements `<div>`
classes  |  `<div class="important">` | `.important {}` | tous les éléments de classe `important`
classes  |  `<div class="important">` | `div .important {}` | tous les éléments `<div>` de classe `important`
clé (id) | `<div id="12345">` | `#12345 {}` | tous les éléments dont l'id est `12345`
clé (id) | `<div id="12345">` | `div #12345 {}` | tous les éléments `<div>` dont l'id est `12345`
etc  | ...   |   |  

- le modèle de boite [sur W3school](https://www.w3schools.com/css/css_boxmodel.asp) ou [sur Mozilla](https://developer.mozilla.org/fr/docs/Apprendre/CSS/Introduction_%C3%A0_CSS/Le_mod%C3%A8le_de_bo%C3%AEte) (plus complet)

## Styler la page d'accueil

Nous avons appliqué une feuille de style à `page01.html`.
Pour finir, faisons la même chose pour la page `index.html`.

Dans le dossier `styles`, créez un nouveau fichier `mystyle.css` et copiez-coller le contenu du fichier [sources/mystyle.css](sources/mystyle.css).

Ouvrez dans Atom le document `index.html` et supprimez les balises de commentaires autour de l'élément `<link>`. Le début du document `index.html` doit maintenant se présenter ainsi :

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="style/mystyle.css">

    <title>Page d'accueil de Nicolas</title>

  </head>

  <body>
...
```

Vous pouvez maintenant ouvrir le document `index.html` dans votre navigateur.

Dans le terminal, nous allons ajouter ces modifications au registre :

```bash
git add .
git commit -m "ajout d'une feuille de style pour index.html"
```

Vous êtes satisfait des styles de page01.html et index.html ? vous pouvez envoyer le tout sur le serveur :

```bash
git push
```

Sinon, jouez avec les styles autant que vous voulez puis "ajouter et valider" (_add_ et _commit_) les modifications avant de "pousser" vers le serveur.

## Aller plus loin

Le langage CSS consiste à styler les contenus, mais aussi à _agencer_ les blocs d'un document HTML. On appelle cet agencement, le _layout_.

Plusieurs méthodes existent. Nous ne les abordons pas ici, mais je vous invite à découvrir les différentes méthodes dans le tutoriel [CSS Layouts (Mozilla)](https://developer.mozilla.org/fr/docs/Apprendre/CSS/CSS_layout).

## Quelques ressources en ligne

- https://www.w3schools.com/css/default.asp
- https://developer.mozilla.org/fr/docs/Apprendre/CSS
- https://css-tricks.com/
