# Premiers pas en HTML

## On rentre dans le vif

C'est parti pour créer une page web :

1. quelques part dans votre ordinateur, créez un dossier `art6017-prenom` dans lequel vous allez travailler tout au long de l'atelier (remplacez `prenom` par votre prénom..).
2. créez maintenant un fichier `page01.html`. Pour cela, vous pouvez utiliser :
    - soit votre explorateur de fichier : clic-droit "créer un nouveau fichier", et renommez le `page01.html`
    - soit l'éditeur de texte : lancez l'éditeur et créez un nouveau fichier, que vous enregistrez sous page01.html
    - soit la ligne de commande : `touch page01.html`
3. si ce n'est pas déjà fait, ouvrez ce fichier dans votre éditeur de texte, et collez y le code suivant :

    ```html
    <!DOCTYPE html>
    <html>
      <head>
        <meta charset="utf-8">
        <title>page01 - Ma page de test</title>
      </head>
      <body>
        <h1>Ma page de test</h1>
        <p>Bienvenu sur ma page de test.</p>
      </body>
    </html>
    ```

4. Sauvegardez le fichier, puis ouvrez ce fichier dans votre navigateur:
    - explorateur: clic-droit sur le fichier, "Ouvrir avec" > Firefox
    - Firefox : Ouvrir un fichier (Ctrl+O)
    - ligne de commande : `firefox page01.html`

Vous avez créé votre première page web.

## Quelques bases

- [anatomie d'un élément](https://developer.mozilla.org/fr/docs/Apprendre/Commencer_avec_le_web/Les_bases_HTML#Anatomie_d'un_%C3%A9l%C3%A9ment_HTML)
- [éléments basiques](https://developer.mozilla.org/fr/docs/Apprendre/Commencer_avec_le_web/Les_bases_HTML#Anatomie_d'un_document_HTML) `<html>`, `<head>`, `<body>`, `<title>`,
- imbrication des éléments :
    - `<p>Bonjour <strong>Monsieur</strong>.</p>` : <p>Bonjour <strong>Monsieur</strong>.</p>
- éléments vides : `<img src="chaton.jpg"/>`, `<br/>`
- pour aller plus loin [sur les éléments](https://www.w3schools.com/html/html_elements.asp)

## Ajouter du contenu

Pour cette section, voir [Baliser le texte (Mozilla)](https://developer.mozilla.org/fr/docs/Apprendre/Commencer_avec_le_web/Les_bases_HTML#Baliser_le_texte)

- exercice au fur et à mesure : ajoutez du contenu structuré dans votre fichier page01.html : titres, sous-titres, paragraphe.s, liste.s, lien.s vers l'extérieur

### Titres

- éléments `<h1>`, `<h2>`,  `<h3>`, etc. : pour _heading_

### Paragraphe

- élément `<p>` : pour _paragraph_

### Listes

- éléments `<ul>` pour _un-ordered list_, `<ol>` pour _ordered list_
- élément `<li>` pour _list item_

### Liens

- élément `<a>`: pour _anchor_ ou _ancre_

    `Allez voir <a href="http://page.com/que-vous-pointez/">cette page</a>`

- lien externe : `<a href="http://wikipedia.org/">Wikipédia</a>`
- lien interne : `<a href="./page02.html">Page 02</a>`
- ancre : `<a href="#quelques-bases">remonter à la section précédente "Quelques bases"</a>`

### Images

- élément `<img/>`

### Commentaires

- `<!-- Voici un commentaire -->`
- pourquoi commenter le code ?

## Publiez votre code sur framagit

1. initialiser votre dossier `art6017-prenom` en "répertoire git" : `git init`
2. ajouter et valider vos modifications:
    - `git add .`
    - `git commit -m "message pour documenter votre modification"`
1. créer un répertoire sur Framagit, nommé `art6017-prenom`
2. lier votre répertoire local au répertoire framagit
3. "pousser" votre répertoire local vers le serveur (le répertoire framagit): `git push`

## Pour allez plus loin

Vous pouvez désormais explorer et tester toutes les balises html existantes. Vous trouvez ici [une liste exhaustive des balises (W3school.com)](https://www.w3schools.com/tags/default.asp)

Vous pouvez aussi suivre [le tutoriel HTML5 (W3school.com)](https://www.w3schools.com/html/default.asp)

- [ressource de ressources](https://la-cascade.io/ressources/#html)
- [htmlreference.io/](https://htmlreference.io/)
- [structurer une page](https://blog.teamtreehouse.com/use-html5-sectioning-elements)
- [caractères spéciaux](http://alexandre.alapetite.fr/doc-alex/alx_special.html)
